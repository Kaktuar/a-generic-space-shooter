using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField]
    private float speed = 1f;
    [SerializeField]
    private float maxGap = 0f;
    [SerializeField]
    private float minGap = 0f;
    [SerializeField]
    private float yPosVariance = 0f;
    [SerializeField]
    private GameObject sprite;

    private GameObject sprite2;
    private float spriteWidth;
    private float flipValue = 0;
    private float initialYPos;

    void Start()
    {
        speed = speed * 3;
        initialYPos = sprite.transform.position.y;
        var s = sprite.GetComponent<SpriteRenderer>();
        spriteWidth = s.bounds.size.x;

        sprite2 = Instantiate(sprite);
        var sprite2XPos = spriteWidth + Random.Range(minGap, maxGap);
        var sprite2YPos = Random.Range(0, yPosVariance) - yPosVariance / 2;
        sprite2.transform.parent = this.transform;
        sprite2.transform.position = sprite.transform.position + new Vector3(sprite2XPos, sprite2YPos, 0);
        flipValue = -sprite2XPos;
    }

    void Update()
    {
        if (sprite.transform.position.x < flipValue)
        {
            var nextXPos = spriteWidth + Random.Range(minGap, maxGap);
            var nextYPos = Random.Range(0, yPosVariance) - yPosVariance / 2;

            flipValue = -nextXPos;
            sprite.transform.position = new Vector3(sprite2.transform.position.x + nextXPos, initialYPos + nextYPos, 0);
        }
        if (sprite2.transform.position.x < flipValue)
        {
            var nextXPos = spriteWidth + Random.Range(minGap, maxGap);
            var nextYPos = Random.Range(0, yPosVariance) - yPosVariance / 2;

            flipValue = -nextXPos;
            sprite2.transform.position = new Vector3(sprite.transform.position.x + nextXPos, initialYPos + nextYPos, 0);
        }
        sprite.transform.position += Vector3.left * Time.deltaTime * speed;
        sprite2.transform.position += Vector3.left * Time.deltaTime * speed;
    }
}
