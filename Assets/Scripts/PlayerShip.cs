﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class PlayerShip : Ship
{
	private bool IsInvincible { get; set; } = false;
	private float InvincibilityTime { get; set; } = 1.5f;
	public bool IsManeuverable { get; private set; } = false;

	private void OnTriggerEnter2D(Collider2D other)
	{
		var enemy = other.gameObject.GetComponent<EnemyShip>();
		if (enemy != null)
		{
			Hit();
		}
	}

	public override void Hit()
	{
		if (!IsInvincible)
		{
			HealthPoint--;
			if (HealthPoint == 0)
			{
				OnDeath();
				Destroy(this.gameObject);
				GameManager.Instance.GameState = GameState.Dead;
			}
			else
			{
				OnHurt();
				StartCoroutine(Invincibility());
			}
		}
	}

	IEnumerator Invincibility()
	{
		IsInvincible = true;
		yield return new WaitForSeconds(0.1f);
		var renderer = this.GetComponent<Renderer>();
		var t = Time.time + InvincibilityTime;
		while (Time.time < t)
		{
			renderer.enabled = false;
			yield return new WaitForSeconds(0.24f);
			renderer.enabled = true;
			yield return new WaitForSeconds(0.24f);
		}
		IsInvincible = false;
	}
}
