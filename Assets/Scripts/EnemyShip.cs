using System.Collections;
using UnityEngine;

public class EnemyShip : Ship
{
	[SerializeField]
	private float speed = 2f;
	[SerializeField]
	private int enemyScore = 100;
	[SerializeField]
	private string enemyName;
	[SerializeField]
	private float potentialShootNbrPerSec = 2f;

	public string EnemyName { get { return enemyName; } }
	private BulletGun bulletGun;

	public Vector2 Position
	{
		get
		{
			return this.transform.position;
		}
		set
		{
			this.transform.position = value;
		}
	}

	public void Initialise(Vector3 position)
	{
		transform.position = position;
		HealthPoint = maxHealthPoint;
	}

	protected override void Start()
	{
		HealthPoint = maxHealthPoint;
		// TODO Potentially move to Ship.cs if a bulletGun ref is needed in PlayerShip
		bulletGun = GetComponent<BulletGun>();
		if (bulletGun == null)
		{
			Debug.LogError("EnemyShip : Can't retrieve a BulletGun component on the gameobject");
		}
		// TODO Shoot AI
		StartCoroutine(TryShoot());
	}
	
	void Update()
	{
		// TODO Engine AI
		transform.position += Vector3.left * Time.deltaTime * this.speed;
		if (this.Position.x < -15 || this.Position.x > 15 || this.Position.y > 15 || this.Position.y < -15)
		{
			EnemiesFactory.ReleaseEnemy(this);
		}
	}

	IEnumerator TryShoot()
	{
		var timeBetweenTries = 0.05f;
		while (true)
		{
			if (!bulletGun.IsFiring)
			{
				yield return new WaitForSeconds(timeBetweenTries);
				var rand = UnityEngine.Random.Range(0, (int)(potentialShootNbrPerSec / timeBetweenTries));
				if (rand == 0)
				{
					bulletGun.Fire();
				}
			}
			else
				yield return null;
		}
	}

	public override void Hit()
	{
		HealthPoint--;
		if (HealthPoint == 0)
		{
			OnDeath();
			EnemiesFactory.ReleaseEnemy(this);
			GameManager.Instance.Score += enemyScore;
		}
	}
}
