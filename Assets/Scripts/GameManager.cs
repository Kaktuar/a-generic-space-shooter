﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[SerializeField]
	private GameObject playerPrefab;

	public static GameManager Instance { get; private set; }
	public PlayerShip PlayerShip { get; private set; }

	public int Score { get; set; }
	public int HighScore { get; private set; }
	public GameState GameState { get; set; }
	public bool Pause { get; set; } = false;
	public static bool DisplayFirstRunMenu { get; private set; } = true;

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("Multiple instance of singleton GameManager");
			return;
		}

		Instance = this;
	}

	void Start()
	{
		if (DisplayFirstRunMenu)
		{
			PauseGame();
		}
		this.PlayerShip = GameObject.Instantiate(playerPrefab).GetComponent<PlayerShip>();
		if (this.PlayerShip == null)
		{
			Debug.LogError("Can't retrieve PlayerShip component");
		}
		this.HighScore = PlayerPrefs.GetInt("HighScore", 0);
	}

	void Update()
	{
		if (Score > HighScore)
		{
			HighScore = Score;
			PlayerPrefs.SetInt("HighScore", HighScore);
			Debug.Log(HighScore);
		}
	}

	public void Restart()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(0);
	}

	public void PauseGame()
	{
		GameState = GameState.Pause;
		Time.timeScale = 0;
	}

	public void ContinueGame()
	{
		Time.timeScale = 1;
		GameState = GameState.Playing;
	}

	public void NoLongerFirstRun()
	{
		DisplayFirstRunMenu = false;
	}
}
