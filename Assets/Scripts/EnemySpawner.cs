﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private List<EnemySpawnProperty> enemySpawnProperties = new List<EnemySpawnProperty>();

    private int _spawnProbabilitySum;
    private int SpawnProbabilitiesSum
    {
        get
        {
            if (_spawnProbabilitySum == 0)
            {
                var tot = 0;
                foreach (var enemy in enemySpawnProperties)
                {
                    tot += enemy.spawnProbability;
                }
                _spawnProbabilitySum = tot;
            }
            return _spawnProbabilitySum;
        }
    }

    private GameObject Draw()
    {
        var rand = Random.Range(0, this.SpawnProbabilitiesSum);
        var prob = 0;
        foreach (var enemy in enemySpawnProperties)
        {
            prob += enemy.spawnProbability;
            if (rand < prob)
            {
                return enemy.prefab;
            }
        }
        return null;
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 2f));
            var prefab = Draw();
            EnemiesFactory.GetEnemy(new Vector2(this.transform.position.x, Random.Range(-4.3f, 4.3f) + this.transform.position.y), prefab);
        }
    }

    void Start()
    {
        StartCoroutine(Spawn());
    }

    protected void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Helper.DrawRectancle(new Vector2(transform.position.x, transform.position.y) - new Vector2(0.5f, 0.5f), Vector2.one);
    }

    [System.Serializable]
    private class EnemySpawnProperty
    {
        public GameObject prefab;
        public int spawnProbability;
    }
}

