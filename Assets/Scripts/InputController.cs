﻿using UnityEngine;

[RequireComponent(typeof(PlayerEngine))]
[RequireComponent(typeof(BulletGun))]
public class InputController : MonoBehaviour
{
	private BulletGun bulletGun;
	private PlayerEngine playerEngine;

	void Start()
	{
		playerEngine = GetComponent<PlayerEngine>();
		if (playerEngine == null)
		{
			Debug.LogError("Can't retrieve a PlayerEngine component on the gameobject");
		}
		bulletGun = GetComponent<BulletGun>();
		if (bulletGun == null)
		{
			Debug.LogError("Can't retrieve a BulletGun component on the gameobject");
		}
	}

	void Update()
	{
		float horizontalSpeedPercent = Input.GetAxis("Horizontal");
		float verticalSpeedPercent = Input.GetAxis("Vertical");
		if (GameManager.Instance.GameState == GameState.Playing && playerEngine.IsManeuverable)
		{
			playerEngine.SetPosition(horizontalSpeedPercent, verticalSpeedPercent);
			if (Input.GetButton("Fire1"))
			{
				bulletGun.Fire();
			}
		}
		if (Input.GetButtonDown("Cancel") && !GameManager.DisplayFirstRunMenu)
		{
			if (GameManager.Instance.GameState == GameState.Pause)
				GameManager.Instance.ContinueGame();
			else if (GameManager.Instance.GameState == GameState.Playing)
				GameManager.Instance.PauseGame();
		}
	}

}
