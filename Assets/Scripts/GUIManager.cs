﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GUIManager : MonoBehaviour
{
	[SerializeField]
	private GameObject pauseMenu;
	[SerializeField]
	private GameObject gameOverMenu;
	[SerializeField]
	private GameObject titleScreen;
	[SerializeField]
	private TextMeshProUGUI lifeNbr;
	[SerializeField]
	private TextMeshProUGUI gameOverScore;
	[SerializeField]
	private TextMeshProUGUI actualScore;
	[SerializeField]
	private TextMeshProUGUI pauseBestScore;
	[SerializeField]
	private TextMeshProUGUI gameOverBestScore;
	[SerializeField]
	private Image titleScreenMuteButton;
	[SerializeField]
	private Image gameOverMuteButton;
	[SerializeField]
	private Image pauseMuteButton;
	[SerializeField]
	private Sprite speakerIcon;
	[SerializeField]
	private Sprite mutedSpeakerIcon;

	void Update()
	{
		lifeNbr.text = GameManager.Instance.PlayerShip.HealthPoint.ToString();
		actualScore.text = GameManager.Instance.Score.ToString("0000000");
		if (GameManager.DisplayFirstRunMenu)
		{
			titleScreen.SetActive(true);
		}
		else if (GameManager.Instance.GameState == GameState.Dead)
		{
			gameOverMenu.SetActive(true);
			gameOverScore.text = GameManager.Instance.Score.ToString("0000000");
			gameOverBestScore.text = GameManager.Instance.HighScore.ToString("0000000");
		}
		else if (GameManager.Instance.GameState == GameState.Playing)
		{
			gameOverMenu.SetActive(false);
			pauseMenu.SetActive(false);
			titleScreen.SetActive(false);
		}
		else if (GameManager.Instance.GameState == GameState.Pause)
		{
			pauseBestScore.text = GameManager.Instance.HighScore.ToString("0000000");
			pauseMenu.SetActive(true);
		}
		gameOverMuteButton.sprite = AudioManager.Instance.IsMuted ? mutedSpeakerIcon : speakerIcon;
		pauseMuteButton.sprite = AudioManager.Instance.IsMuted ? mutedSpeakerIcon : speakerIcon;
		titleScreenMuteButton.sprite = AudioManager.Instance.IsMuted ? mutedSpeakerIcon : speakerIcon;
	}
}
