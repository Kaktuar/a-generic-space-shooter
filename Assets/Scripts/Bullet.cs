﻿using UnityEngine;

public enum BulletType
{
    PlayerBullet,
    EnemyBullet
}

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private BulletType type;
    [SerializeField]
    [Range(0f, 50f)]
    private Vector2 speed;

    public BulletType Type
    {
        get { return this.type; }
    }

    public Vector2 Position
    {
        get
        {
            return this.transform.position;
        }

        set
        {
            this.transform.position = value;
        }
    }

    void Update()
    {
        this.Position += this.speed * Time.deltaTime;
        if (this.Position.x < -15 || this.Position.x > 15 || this.Position.y > 15 || this.Position.y < -15)
        {
            BulletsFactory.ReleaseBullet(this);
        }
    }

    public void Initialize(Vector2 direction, float speed)
    {
        this.speed = direction * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Ship ship = null;
        switch (this.type)
        {
            case BulletType.PlayerBullet:
                ship = other.gameObject.GetComponent<EnemyShip>();
                break;
            case BulletType.EnemyBullet:
                ship = other.gameObject.GetComponent<PlayerShip>();
                break;
        }
        if (ship != null)
        {
            ship.Hit();
            BulletsFactory.ReleaseBullet(this);
        }
    }
}
