using UnityEngine;
using System;

public abstract class Ship : MonoBehaviour
{
	[SerializeField]
	protected int maxHealthPoint = 2;

	public int HealthPoint { get; protected set; }
	public event Action Death = delegate { };
	public event Action Hurt = delegate { };

	protected virtual void Start()
	{
		HealthPoint = maxHealthPoint;
	}

	protected void OnDeath()
	{
		Death();
	}

	protected void OnHurt()
	{
		Hurt();
	}

	public abstract void Hit();
}