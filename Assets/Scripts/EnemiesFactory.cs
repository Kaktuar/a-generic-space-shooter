﻿using System.Collections.Generic;
using UnityEngine;

public class EnemiesFactory : MonoBehaviour
{
    private Dictionary<string, Queue<EnemyShip>> availableEnemiesPerType = new Dictionary<string, Queue<EnemyShip>>();
    private static EnemiesFactory Instance { get; set; }

    public static void ReleaseEnemy(EnemyShip enemy)
    {
        CheckEnemyType(enemy.EnemyName);
        Queue<EnemyShip> availableEnemies = EnemiesFactory.Instance.availableEnemiesPerType[enemy.EnemyName];
        enemy.gameObject.SetActive(false);
        availableEnemies.Enqueue(enemy);
    }

    public static void CheckEnemyType(string name)
    {
        if (!EnemiesFactory.Instance.availableEnemiesPerType.ContainsKey(name))
        {
            EnemiesFactory.Instance.availableEnemiesPerType.Add(name, new Queue<EnemyShip>());
        }
    }

    public static EnemyShip GetEnemy(Vector2 position, GameObject prefab)
    {
        string name = prefab.GetComponent<EnemyShip>().EnemyName;
        CheckEnemyType(name);
        EnemyShip enemy = null;
        Queue<EnemyShip> availableEnemies = EnemiesFactory.Instance.availableEnemiesPerType[name];

        if (availableEnemies.Count > 0)
        {
            enemy = availableEnemies.Dequeue();
            enemy.gameObject.SetActive(true);
        }

        if (enemy == null)
        {
            var go = Instantiate(prefab);
            enemy = go.GetComponent<EnemyShip>();
            enemy.transform.parent = EnemiesFactory.Instance.gameObject.transform;
        }

        enemy.Initialise(position);

        return enemy;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instance of singleton EnemiesFactory");
            return;
        }

        Instance = this;
    }
}
