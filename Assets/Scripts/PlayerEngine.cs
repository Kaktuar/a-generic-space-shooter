using UnityEngine;
using System;
using System.Collections;

public class PlayerEngine : MonoBehaviour
{
	[SerializeField]
	private Rect allowedZone;
	[SerializeField]
	private float maximumSpeed = 11f;
	[HideInInspector]
	public bool IsManeuverable { get; private set; } = false;

	public Vector2 Position
	{
		get
		{
			return this.transform.position;
		}
		private set
		{
			this.transform.position = value;
		}
	}

	void Start()
	{
		StartCoroutine(ShipEmergence());
	}

	public void SetPosition(float hSpeed, float vSpeed)
	{
		var speed = new Vector2(hSpeed, vSpeed);
		Vector2 newPosition = this.Position + (speed * maximumSpeed * Time.deltaTime);
		newPosition = FixPositionIfNotAllowed(newPosition, hSpeed, vSpeed);
		this.Position = newPosition;
	}

	Vector3 FixPositionIfNotAllowed(Vector3 pos, float hSpeed, float vSpeed)
	{
		var left = allowedZone.position.x;
		var right = left + allowedZone.width;
		var bottom = allowedZone.position.y;
		var top = bottom + allowedZone.height;
		var posSensLess = this.Position + (new Vector2(Mathf.Sign(hSpeed), Mathf.Sign(vSpeed)) * maximumSpeed * Time.deltaTime);
		if ((posSensLess.x < left || posSensLess.x > right) && (posSensLess.y < bottom || posSensLess.y > top))
		{
			return this.Position;
		}
		if (posSensLess.x < left || posSensLess.x > right)
		{
			var speed = new Vector2(0, vSpeed);
			return this.Position + (speed * this.maximumSpeed * Time.deltaTime);
		}
		if (posSensLess.y < bottom || posSensLess.y > top)
		{
			var speed = new Vector2(hSpeed, 0);
			return this.Position + (speed * this.maximumSpeed * Time.deltaTime);
		}
		return pos;
	}

	IEnumerator ShipEmergence()
	{
		while (transform.position.x < -7.5)
		{
			transform.position += new Vector3(1, 0, 0) * Time.deltaTime * 10;
			yield return null;
		}
		IsManeuverable = true;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		GUI.contentColor = Color.blue;
		// Handles.Label(allowedZone.position + new Vector2(0, allowedZone.height), "Allowed zone");
		Gizmos.DrawLine(allowedZone.position, allowedZone.position + new Vector2(0, allowedZone.height));
		Gizmos.DrawLine(allowedZone.position, allowedZone.position + new Vector2(allowedZone.width, 0));
		Gizmos.DrawLine(allowedZone.position + new Vector2(allowedZone.width, 0), allowedZone.position + new Vector2(allowedZone.width, allowedZone.height));
		Gizmos.DrawLine(allowedZone.position + new Vector2(0, allowedZone.height), allowedZone.position + new Vector2(allowedZone.width, allowedZone.height));
	}
}