﻿using UnityEngine;

public static class Helper
{
    public static void DrawRectancle(Vector2 p, Vector2 dim)
    {
        var w = new Vector2(dim.x, 0);
        var h = new Vector2(0, dim.y);
        Gizmos.DrawLine(p, p + h);
        Gizmos.DrawLine(p, p + w);
        Gizmos.DrawLine(p + h, p + dim);
        Gizmos.DrawLine(p + w, p + dim);
    }
}
