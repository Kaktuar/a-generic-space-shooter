using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager Instance { get; private set; }

    public bool IsMuted { get; private set; } = false;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instance of singleton AudioManager");
            return;
        }

        Instance = this;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    void Start()
    {
        Play("Theme");
    }

    public static void Play(string name)
    {
        if (name != null)
        {
            Sound s = Array.Find(Instance.sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound " + name + " not found");
                return;
            }
            s.source.Play();
        }
        else Debug.Log("hey");
    }

    public static Sound GetSound(string name)
    {
        Sound s = Array.Find(Instance.sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found");
            return null;
        }
        return s;
    }

    public void Mute()
    {
        IsMuted = !IsMuted;
        foreach (Sound s in sounds)
        {
            s.source.volume = IsMuted ? 0 : s.volume;
        }
    }
}
