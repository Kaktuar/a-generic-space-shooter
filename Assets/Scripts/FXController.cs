using UnityEngine;

public class FXController : MonoBehaviour
{
	[SerializeField]
	private string deathSoundEffect;
	[SerializeField]
	private string shootSoundEffect;
	[SerializeField]
	private string hurtSoundEffect;
	[SerializeField]
	private GameObject explosion;

	private BulletGun bulletGun;
	private Ship ship;

	protected virtual void Start()
	{
		ship = GetComponent<Ship>();
		if (ship == null)
		{
			Debug.LogError("FXCntroller : Can't retrieve a Ship component on the gameobject");
		}
		else
		{
 			ship.Death += Death;
			ship.Hurt += Hurt;
		}
		bulletGun = GetComponent<BulletGun>();
		if (bulletGun == null)
		{
			Debug.LogError("FXController : Can't retrieve a BulletGun component on the gameobject");
		}
		else
		{
			bulletGun.Shoot += Shoot;
		}
	}

	void Shoot()
	{
		AudioManager.Play(shootSoundEffect);
	}

	void Death()
	{
		AudioManager.Play(deathSoundEffect);
		var exp = Instantiate(explosion);
		exp.transform.position = this.transform.position;
		var animationDuration = exp.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
		Destroy(exp, animationDuration);
	}

	void Hurt()
	{
		AudioManager.Play(hurtSoundEffect);
	}
}
