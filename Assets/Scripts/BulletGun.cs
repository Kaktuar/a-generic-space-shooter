﻿using System.Collections;
using System;
using UnityEngine;

public class BulletGun : MonoBehaviour
{
    [SerializeField]
    private BulletType bulletType;
    [SerializeField]
    private float fireRate = 5;
    [SerializeField]
    private float speed = 5;
    [SerializeField]
    private Vector2 direction;
    [SerializeField]
    private Vector3 relativePosition;

    public event Action Shoot = delegate { };
    private float lastFireTime = 0f;
    public bool IsFiring { get; private set; } = false;

    public void Fire()
    {
        if (!IsFiring)
        {
            var bullet = BulletsFactory.GetBullet(transform.position + relativePosition, bulletType);
            bullet.Initialize(direction, speed);
            StartCoroutine(AssertFireRate());
            IsFiring = true;
            Shoot();
        }
    }

    IEnumerator AssertFireRate()
    {
        if (fireRate > 0f)
        {
            float durationBetweenTwoBullets = 1f / this.fireRate;
            float t = 0;
            while (t < durationBetweenTwoBullets)
            {
                t += Time.deltaTime;
                yield return null;
            }
            IsFiring = false;
        }
        else
        {
            IsFiring = false;
        }
    }

    protected void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position + relativePosition, transform.position + relativePosition + new Vector3(direction.x, direction.y, 0), Color.red);
    }
}
