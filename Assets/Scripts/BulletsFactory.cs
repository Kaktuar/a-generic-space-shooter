﻿using System.Collections.Generic;
using UnityEngine;

public class BulletsFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject playerBulletPrefab;
    [SerializeField]
    private GameObject enemyBulletPrefab;
    private Dictionary<BulletType, Queue<Bullet>> availableBulletsPerType = new Dictionary<BulletType, Queue<Bullet>>();

    private static BulletsFactory Instance { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instance of singleton BulletsFactory");
            return;
        }

        Instance = this;

        foreach (object value in System.Enum.GetValues(typeof(BulletType)))
        {
            this.availableBulletsPerType.Add((BulletType)value, new Queue<Bullet>());
        }
    }

    private static Bullet InstantiateBullet(BulletType bulletType)
    {
        GameObject gameObject = null;

        switch (bulletType)
        {
            case BulletType.EnemyBullet:
                gameObject = Instantiate(BulletsFactory.Instance.enemyBulletPrefab);
                break;
            case BulletType.PlayerBullet:
                gameObject = Instantiate(BulletsFactory.Instance.playerBulletPrefab);
                break;
        }

        var bullet = gameObject.GetComponent<Bullet>();
        bullet.transform.parent = BulletsFactory.Instance.gameObject.transform;
        return bullet;
    }

    public static Bullet GetBullet(Vector2 position, BulletType bulletType)
    {
        Queue<Bullet> availableBullets = BulletsFactory.Instance.availableBulletsPerType[bulletType];

        Bullet bullet = null;
        if (availableBullets.Count > 0)
        {
            bullet = availableBullets.Dequeue();
            bullet.gameObject.SetActive(true);
        }

        if (bullet == null)
        {
            bullet = InstantiateBullet(bulletType);
        }

        bullet.Position = position;

        return bullet;
    }

    public static void ReleaseBullet(Bullet bullet)
    {
        Queue<Bullet> availableBullets = BulletsFactory.Instance.availableBulletsPerType[bullet.Type];
        bullet.gameObject.SetActive(false);
        availableBullets.Enqueue(bullet);
    }
}
